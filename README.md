# ansible-observium

Very incomplete. Don't even try to use this yet. 

## Variables

The following varibles must be specified for each host in the inventory file.

### Observium group only
- dbrootpw - The MariaDB root password.
- dbuserpw - The MariaDB "observium" user password.
- adminun - The Observium admin username.
- adminpw - The Observium admin password.

### Both Observium and snmpd groups
- authpass - The SNMPv3 auth password.
- authtype - The SNMPv3 auth type (i.e. MD5 or SHA).
- privpass - The SNMPv3 priv password.
- privtype - The SNMPv3 priv type (i.e. AES or DES).
- user - The SNMPv3 username.
